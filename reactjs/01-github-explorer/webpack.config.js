const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ReactRefreshWebPackPlugin = require('@pmmmwh/react-refresh-webpack-plugin')

const isDevelopment = process.env.NODE_ENV !== 'production'

module.exports = {
  mode: isDevelopment ? 'development' : 'production',
  devtool: isDevelopment ? 'eval-source-map' : 'source-map',
  entry: path.resolve(__dirname, 'src', 'index.tsx'), //entry point
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx'] //extensions to parse
  },
  devServer: {
    contentBase: path.resolve(__dirname, 'public'),
    hot: true, //activate hot refresh
  },
  plugins: [
    isDevelopment && new ReactRefreshWebPackPlugin({ //if it's production, return false

    }
    ),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'public', 'index.html') //auto load webpack into index.html
    })
  ].filter(Boolean), //remove "false" because "false" is an invalid plugin object
  module: {
    rules: [
      {
        test: /\.(j|t)sx$/, //select files ending in .jsx or .tsx
        exclude: /node_modules/, //exclude node_modules dir
        use: {
          loader: 'babel-loader', //use babel-loader in jsx files
          options: {
            plugins: [
              isDevelopment && require.resolve('react-refresh/babel') //use react-refresh babel plugin in dev mode
            ].filter(Boolean)
          }
        }
      },
      {
        test: /\.scss$/, //select files ending in .css
        exclude: /node_modules/, //exclude node_modules dir
        use: ['style-loader', 'css-loader', 'sass-loader'], //use style-loader and css-loader in css files
      }
    ]
  }
}