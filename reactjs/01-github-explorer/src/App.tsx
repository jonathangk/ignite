import { Counter } from './components/Counter'
import { RepositoryList } from './components/RepositoryList'
import './styles/global.scss'

export function App() {
  return (
    <>
      {/* fragment no React, é como se fosse uma div mas não vira div no html */}
      <RepositoryList />
      {/* <Counter /> */}
    </>
  )
}