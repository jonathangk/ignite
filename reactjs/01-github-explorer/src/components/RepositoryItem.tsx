interface RepositoryItemProps {
  repository: {
    id: string;
    name: string;
    description: string;
    http_url_to_repo: string;
  }
}

export function RepositoryItem(props: RepositoryItemProps) {
  return (
    <li>
      <strong>{props.repository?.name ?? 'Repository without name'}</strong>
      {/* ? no objeto somente acessa propriedade se objeto existir */}
      <p>{props.repository?.description ?? 'Repository without description'}</p>
      <a href={props.repository?.http_url_to_repo ?? ''}>
        Acessar repositório
      </a>
    </li>
  )
}
