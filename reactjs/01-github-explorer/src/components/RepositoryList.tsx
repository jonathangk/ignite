import { useState, useEffect } from 'react'
import { RepositoryItem } from "./RepositoryItem"

import '../styles/repositories.scss'

interface Repository {
  id: string
  name: string
  description: string
  http_url_to_repo: string
}

export function RepositoryList() {
  const [repositories, setRepositories] = useState<Repository[]>([])

  //useEffect, dispara uma funcao quando algo acontece na aplicacao (observer)
  useEffect(() => {
    fetch('https://gitlab.com/api/v4/groups/11327468/projects')
      .then(response => response.json())
      .then(data => setRepositories(data))
  }, []) //[] array em branco, executa uma unica vez

  return (
    <section className="repository-list">
      <h1>Lista de repositórios</h1>

      <ul>
        {repositories.map(repository => {
          return <RepositoryItem key={repository.id} repository={repository} />
          //as propriedades do objeto repository viram props, no RepositoryItem
          //basta utilizar as props com o nome do objeto
          //a propriedade key é necessaria para o React identificar cada elemento
          //e saber quando atualiza determinados elementos em especifico
        })}
      </ul>
    </section>
  )
}