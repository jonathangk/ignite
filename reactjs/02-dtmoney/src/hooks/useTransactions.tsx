import { createContext, ReactNode, useContext, useEffect, useState } from "react";
import { api } from "../services/api";

interface Transaction {
  id: number
  title: string
  amount: number
  type: string
  category: string
  createdAt: string
}

interface TransactionsProviderProps {
  children: ReactNode
}

// interface TransactionInput { //MORE VERBOSE
//   title: string
//   amount: number
//   type: string
//   category: string
// }
type TransactionInput = Omit<Transaction, 'id' | 'createdAt'> //LESS VERBOSE
// type TransactionInput = Pick<Transaction, 'title' | 'amount' | 'type' | 'category'> //ANOTHER ALTERNATIVE

interface TransactionsContextData {
  transactions: Transaction[],
  createTransaction: (transaction: TransactionInput) => Promise<void>
}

const TransactionsContext = createContext<TransactionsContextData>({} as TransactionsContextData)

export function TransactionsProvider({ children }: TransactionsProviderProps) {
  const [transactions, setTransactions] = useState<Transaction[]>([])

  useEffect(() => {
    api.get('transactions')
      .then(response => {
        setTransactions(response.data.transactions)
      })
  }, [])

  async function createTransaction(transactionInput: TransactionInput) {
    const response = await api.post('/transactions', transactionInput)
    const { transaction } = response.data

    setTransactions([...transactions, transaction])
  }

  return (
    //duas chaves, primeira pra dizer que é um JS, a segunda é um objeto
    <TransactionsContext.Provider value={{ transactions, createTransaction }}>
      {children}
    </TransactionsContext.Provider>
  )
}

export function useTransactions() {
  const context = useContext(TransactionsContext)

  return context
}